#!/bin/bash

# stop immediatly if the script fails
set -e

# create the databases
psql -v ON_ERROR_STOP=1 --username "postgres" --dbname "postgres" <<-EOSQL
  CREATE DATABASE cartostation;
EOSQL

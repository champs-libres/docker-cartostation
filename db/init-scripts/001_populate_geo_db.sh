#!/bin/bash

# stop immediatly if the script fails
set -e

# execute sql in the database cartostation
cat /docker-entrypoint-initdb.d/municipalities.sql.data | psql -v ON_ERROR_STOP=1 --username "${POSTGRES_USER}" --dbname "cartostation"
cat /docker-entrypoint-initdb.d/railways.sql.data | psql -v ON_ERROR_STOP=1 --username "${POSTGRES_USER}" --dbname "cartostation"
cat /docker-entrypoint-initdb.d/school_buildings.sql.data | psql -v ON_ERROR_STOP=1 --username "${POSTGRES_USER}" --dbname "cartostation"
cat /docker-entrypoint-initdb.d/trees.sql.data | psql -v ON_ERROR_STOP=1 --username "${POSTGRES_USER}" --dbname "cartostation"

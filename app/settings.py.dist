# call base settings
import os
from main.settings import *

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# a secret key
SECRET_KEY = 'dsqdm7&ot6xsr&-isncic2kve$l@r)^=0n)f5+*rgn2)kki5)^c0mkqsdqsd'

# actual allowed hosts
ALLOWED_HOSTS = ['10.0.0.1', 'localhost']

CORS_ORIGIN_ALLOW_ALL = True
SESSION_COOKIE_SAMESITE = None

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.gis",
    "api",
    "clients",
    "documents",
    "timeserie",
    "catalog",
    "webservice",
    "web",
    "remote_manage",
    # 'render',
    "rest_framework",
    "rest_framework_gis",
    "corsheaders",
    "lingua",
    "activity",
    "basic_wfs",
    "lookup",
    "online",
    "rules.apps.AutodiscoverRulesConfig",
    "django_admin_search",
    "sorl.thumbnail",
]

INSTALLED_APPS.append('postgis_loader')

SITE_ID = 1

STATIC_URL = '/static/'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# an actual database setting
DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'HOST': 'db',
        'PORT': 5432,
        'NAME': 'cartostation',
        'PASSWORD': 'postgres',
        'USER': 'postgres',
    },
}

# specific config for layers database
LAYERS_DB = {
    'ENGINE': 'django.contrib.gis.db.backends.postgis',
    'HOST': 'db',
    'NAME': 'cartostation',
    'PASSWORD': 'postgres',
    'USER': 'postgres',
}

# here's a little loop to point to your postgis schemas
LAYERS_SCHEMAS = [
    'gis'
]

for schema in LAYERS_SCHEMAS:
    db_config = LAYERS_DB.copy()
    db_config.update({
        'OPTIONS': {
            'options': '-c search_path={},public'.format(schema),
        },
    })
    DATABASES[schema] = db_config

# check null geometries in serializer
GEOM_NOT_NULL_SETTING = True

# clients are declared here
CLIENTS = [
    {
        'route' : 'alias',
        'name' : {
            'fr': 'Alias',
            'nl': 'Alias',
        }
    },
    {
        'route' : 'compose',
        'name' : {
            'fr': 'Studio',
            'nl': 'Studio',
        }
    },
    {
        'route' : 'dashboard',
        'name' : {
            'fr': 'Dashboard',
            'nl': 'Dashboard',
        }
    },
    {
        'route' : 'metadata',
        'name' : {
            'fr': 'Metadata',
            'nl': 'Metadata',
        }
    },
    {
        'route' : 'view',
        'name' : {
            'fr': 'Atlas',
            'nl': 'Atlas',
        }
    },
    {
        'route' : 'embed'
    },
    {
        'route' : 'login'
    },
]

# default application to serve at index
CLIENTS_DEFAULT = 'dashboard'

MAX_DECIMAL_DIGITS = 3

AUTHENTICATION_BACKENDS = (
    'rules.permissions.ObjectPermissionBackend',
    'django.contrib.auth.backends.ModelBackend',
)

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'main.urls'

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "APP_DIRS": True,
        "DIRS": [os.path.join(BASE_DIR, "main/templates")],
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = 'main.wsgi.application'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME':
        'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# a set of useful URLs to be displayed in client applications
CLIENTS_URLS = [
    {
        'name': 'contact',
        'label': {
            'fr': 'Contact',
            'nl': 'Contact',
        },
        'url': {
            'fr': '/web/fr/contact.html',
            'nl': '/web/nl/contact.html',
        }
    }
]


MAX_DECIMAL_DIGITS = 2

DEFAULT_GROUP = 'sdi:geodata'
PUBLIC_GROUP = 'sdi:public'


# Sorry, but we really need cache
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '/src/carto-station/sdi/cache/sdi',
        'OPTIONS': {
            'MAX_ENTRIES': 200000
        }
    },
    'layers': {
        'BACKEND': 'diskcache.DjangoCache',
        'LOCATION': '/src/carto-station/sdi/cache/sdi/layers',
        'TIMEOUT': 60 * 60 * 24,
        'SHARDS': 4,
        'DATABASE_TIMEOUT': 1.0,
        'OPTIONS': {
            'size_limit': 2**32  # 4 gigabytes
        }
    }
}

# a django setting
MEDIA_ROOT = '/var/cartostation/media'

LANGUAGES = [
    ('fr', 'French'),
    ('nl', 'Dutch'),
]

LANGUAGE_CODE = 'fr'

# enable some logs
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'root': {
        'handlers': ['console'],
        'level': 'DEBUG' if DEBUG else 'WARNING',
    },
}

LOOKUP_SERVICES = [
    {
        'type': 'proxy',
        'name': {
            'fr':'urbis',
            'nl':'urbis_nl'
            },
        'placeholder': {
            'fr':'Encodez l\'adresse',
            'nl':'Schrijf de adres in'
            },
        'description':{
            'fr':'',
            'nl':''
        },
        'config': {
            'module': 'lookup.urbis'
        },
    },
]
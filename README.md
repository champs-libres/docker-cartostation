# docker-cartostation

Implementation of CartoStation with docker-compose.

This repo contains a docker-compose file that install cartostation from source and build containers for developping a cartostation app.

## Installation

1) Clone this repo

```bash
git clone git@gitlab.com:champs-libres/docker-cartostation.git
cd docker-cartostation
```

2) Fetch the submodule, that is, the cartostation repo

```bash
git submodule init
git submodule update
```

3) Build the container

```bash
docker-compose up
```

A pgadmin client already lives at http://localhost:8081/ with UN blop@blop.com and PW blipblop. You should be able to see the "cartostation" database but it will be empty.

## Post-installation

Install postgis-loader

```bash
docker-compose exec --user root app pip install -e ../../postgis-loader
```

Execute Django migrations

```bash
docker-compose exec app python3 manage.py migrate
```

Create a Django superuser

```bash
docker-compose exec app python3 manage.py createsuperuser
```

Now you should be able to have access to the admin at http://localhost:8000/admin (maybe restart the containers once).

Build client applications

```bash
docker-compose run --rm app-clients npm i
docker-compose run --rm app-clients ./build.sh login dashboard view compose alias embed metadata
docker-compose stop
docker-compose up
```

Voilà, you should have a webpage showing cartostation on http://localhost:8000/.

## Working with submodules

On a fresh install, submodules projects (i.e., cartostation and cs-ibge) are fetched from their repo with `git submodule init && git submodule update`. When modifying these projects, you should cd to these projects and commit there:

```bash
cd app/src/carto-station
git pull
git add .
git commit -m ""
git push
```

NB: For just pulling these submodules, you can either go to each directory as described above and make `git pull` or do it for all submodules using `git submodule foreach git pull`.

## Architecture

The project is build around dockers containers :

- some containers are used for running the app & db. They have to run for using the solution.
- some others are used for processing some punctual tasks. It is possible to use the solution even if they are not running.

The containers that serve the solution are :

- `db` : hosting the databases
- `app` : hosting and serving the django app
- `pgadmin` hosting the tool `pgadmin`

The containers that process punctual tasks are :

- `app-clients` : used to build the clients (typescript applications)

The `settings.py` file of the Django app is mounted from `app/settings.py.dist` to the right location in the app sources.


## To enter the containers

### Example, in the db container, to list all databases in the db

```bash
docker-compose exec --user 1000 db bash
bash-5.1$ psql -U postgres
postgres=# \l
```

### Example, in the app container, to execute a migration

```bash
docker-compose exec --user 1000 app bash
I have no name!@3b16cdb96881:/src/carto-station/sdi$ python3 manage.py runserver
```

### Example, in the app container, to add a missing Python dependency (a root access is needed)

```bash
docker-compose exec --user root app bash
root@3b16cdb96881:/src/carto-station/sdi$ pip install sorl-thumbnail
```

### Exemple, in the app-clients container, to build/watch a client you're working on

```bash
docker-compose run --rm app-clients bash
I have no name!@92d42ffb06b0:/src/carto-station/clients$ ./watch.sh view
```
